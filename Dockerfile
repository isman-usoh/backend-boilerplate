# ---- Base ----
FROM node:8-alpine AS base
RUN mkdir -p /usr/app
WORKDIR /usr/app
COPY package.json /usr/app
COPY yarn.lock /usr/app

# ---- Dependencies ----
FROM base AS dependencies
RUN yarn --production
RUN cp -R node_modules prod_node_modules
RUN yarn

# ---- Testing ----
# FROM base AS testing
# COPY --from=dependencies /usr/app/node_modules /usr/app/node_modules
# COPY . /usr/app
# RUN yarn test

# ---- Builder ----
FROM base AS builder
COPY --from=dependencies /usr/app/node_modules /usr/app/node_modules
COPY . /usr/app
RUN yarn build

# ---- Running ----
FROM base as running
ENV NODE_ENV=production
COPY --from=dependencies /usr/app/prod_node_modules /usr/app/node_modules
COPY --from=builder /usr/app/dist /usr/app/dist
EXPOSE 9000
CMD yarn run start:prod