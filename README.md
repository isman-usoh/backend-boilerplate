# Backend Boilerplate

## Description

backend-boilerplate

## Prerequirements
- NodeJS
- Docker

## Installation

```bash
# start mysql and phpmyadmin
$ docker-compose up -d
# install dependency
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
yarn run start:prod
```


## Open the app
```bash
# app
$ open http://localhost:9000

# api document
$ open http://localhost:9000/document

# phpmyadmin
$ open http://localhost:8000
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

