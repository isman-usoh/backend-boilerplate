// tslint:disable:max-line-length
export const environment = {
    production: false,

    database: {
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'backend_development',
    },
    mailer: {
        service: 'gmail',
        auth: {
            user: 'yourmail@gmail.com', // your email
            pass: 'password', // your email password
        },
    },
}
