import 'polyfill'

import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import * as path from 'path'

import { AppModule } from 'modules/app/app.module'
import { RolesGuard } from 'modules/common/guards/roles.guard'

process.on('uncaughtException', err => {
  console.error('UncaughtException', err)
})

process.on('unhandledRejection', (err, p) => {
  console.error('UnhandledRejection', err)
})

async function bootstrap() {
  const appPath = process.env.APP_PATH || ''

  const nest = await NestFactory.create(AppModule)
  const rolesGuard = nest.get<RolesGuard>(RolesGuard)

  nest.use('/healthz', (req, res) => res.status(200).send('Ok'))
  nest.useGlobalGuards(rolesGuard)
  nest.useStaticAssets(path.join(__dirname, '../public'), {
    index: false,
    redirect: false,
  })

  const options = new DocumentBuilder()
    .setTitle('Backend Boilerplate')
    .setDescription('Backend boilerplate APIs document')
    .setVersion(process.env.APP_BUILD_VERSION || '1.0')
    .setSchemes(process.env.NODE_ENV === 'production' ? 'https' : 'http')
    .setHost(process.env.APP_HOST || 'localhost:9000')
    .setBasePath(appPath)
    .addBearerAuth('Authorization', 'header')
    .build()

  const document = SwaggerModule.createDocument(nest, options)
  SwaggerModule.setup('/document', nest, document)

  await nest.listen(9000)
}

bootstrap()
