import { Inject, Injectable } from '@nestjs/common'
import { Preemption } from 'modules/preemption/preemption.entity'
import { Repository } from 'typeorm'

@Injectable()
export class PreemptionService {
    constructor(
        @Inject('PreemptionRepository')
        private readonly preemptionRepo: Repository<Preemption>,
    ) {}
}
