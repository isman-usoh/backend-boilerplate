import { Coupon } from 'modules/preemption/coupon.entity'
import { Preemption } from 'modules/preemption/preemption.entity'
import { Connection } from 'typeorm'

export const preemptionProviders = [
    {
        provide: 'PreemptionRepository',
        useFactory: (connection: Connection) => connection.getRepository(Preemption),
        inject: ['DbConnection'],
    },
    {
        provide: 'CouponRepository',
        useFactory: (connection: Connection) => connection.getRepository(Coupon),
        inject: ['DbConnection'],
    },
]
