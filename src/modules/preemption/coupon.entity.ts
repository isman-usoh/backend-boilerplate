import { Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Coupon {
    @PrimaryGeneratedColumn()
    id: number
}
