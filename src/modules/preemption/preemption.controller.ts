import { Controller, Get, HttpCode, Post, Query, UseGuards } from '@nestjs/common'
import { ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { PreemptionDTO } from 'modules/preemption/preemption.dto'
import { PreemptionService } from 'modules/preemption/preemption.service'

@Controller('preemptions')
// @UseGuards(AuthGuard('bearer'))
// @ApiBearerAuth()
@ApiUseTags('Preemption')
export class PreemptionController {
    constructor(private readonly preemptionSvc: PreemptionService) {}

    @Get()
    @HttpCode(200)
    @ApiOperation({ title: 'ดึงข้อมูลใบจองทั้งหมด', operationId: 'get_preemption' })
    @ApiResponse({ status: 200, type: PreemptionDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async find(@Query('q') query: string = '',
               @Query('offet') offset: number = 0,
               @Query('limit') limit: number = 50) {

    }

    @Post()
    @HttpCode(201)
    @ApiOperation({ title: 'สร้างใบจอง', operationId: 'create_preemption'})
    @ApiResponse({ status: 201, type: PreemptionDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async create() {

    }
}
