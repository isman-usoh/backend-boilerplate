import { Module } from '@nestjs/common'
import { CommonModule } from 'modules/common/common.module'
import { PreemptionController } from 'modules/preemption/preemption.controller'
import { preemptionProviders } from 'modules/preemption/preemption.providers'
import { PreemptionService } from 'modules/preemption/preemption.service'

@Module({
    imports: [
        CommonModule,
    ],
    controllers: [
        PreemptionController,
    ],
    providers: [
        PreemptionService,
        ...preemptionProviders,
    ],
})
export class PreemptionModule {

}
