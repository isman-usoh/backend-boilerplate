import { Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Preemption {
    @PrimaryGeneratedColumn()
    id: number
}
