import { ArgumentMetadata, BadRequestException, Injectable, Optional, PipeTransform } from '@nestjs/common'
import * as classTransformer from 'class-transformer'
import * as classValidator from 'class-validator'
import { ValidatorOptions } from 'class-validator'
import { isNil } from 'ramda'

export interface ValidationPipeOptions extends ValidatorOptions {
    transform?: boolean
    disableErrorMessages?: boolean
}

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
    protected isTransformEnabled: boolean
    protected isDetailedOutputDisabled: boolean
    protected validatorOptions: ValidatorOptions

    constructor(@Optional() options?: ValidationPipeOptions) {
        options = options || {}

        const { transform, disableErrorMessages, ...validatorOptions } = options
        this.validatorOptions = validatorOptions
        this.isTransformEnabled = !!transform
        this.isDetailedOutputDisabled = disableErrorMessages
    }

    async transform(value, metadata: ArgumentMetadata) {
        if (!metadata.metatype || !this.toValidate(metadata)) {
            return value
        }
        const entity = classTransformer.plainToClass(
            metadata.metatype,
            this.toEmptyIfNil(value),
        )

        const errors = await classValidator.validate(entity, this.validatorOptions)
        if (errors.length > 0) {
            throw new BadRequestException(
                this.isDetailedOutputDisabled ? undefined : errors,
                'ValidationFailed',
            )
        }
        return this.isTransformEnabled
            ? entity
            : Object.keys(this.validatorOptions).length > 0
                ? classTransformer.classToPlain(entity)
                : value
    }

    private toValidate(metadata: ArgumentMetadata): boolean {
        const { metatype, type } = metadata
        if (type === 'custom') {
            return false
        }
        const types = [String, Boolean, Number, Array, Object]
        return !types.some(t => metatype === t) && !isNil(metatype)
    }

    toEmptyIfNil<T = any, R = any>(value: T): R | {} {
        return isNil(value) ? {} : value
    }
}
