import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger'

export class ErrorDTO {
  @ApiModelProperty() statusCode: number
  @ApiModelProperty() error: string
  @ApiModelPropertyOptional() message?: string
}
