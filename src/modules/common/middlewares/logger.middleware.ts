import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common'
import * as _ from 'lodash'
import { LoggerService } from 'modules/common/services/logger.service'

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    private ignoredRoutes = [
        '/healthz',
    ]

    private msgFormat = '{{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms'

    constructor(private logger: LoggerService) {}

    private levelFromStatus(req, res) {
        let level = ''
        if (res.statusCode >= 100) { level = 'info' }
        if (res.statusCode >= 400) { level = 'warn' }
        if (res.statusCode >= 500) { level = 'error' }
        return level
    }

    resolve(...args: any[]): MiddlewareFunction {
        return (req, res, next) => {
            const currentUrl = req.originalUrl || req.url
            if (currentUrl && _.includes(this.ignoredRoutes, currentUrl)) return next()

            req._startTime = new Date().getTime()

            const end = res.end
            res.end = (chunk, encoding) => {
                res.responseTime = new Date().getTime() - req._startTime
                res.end = end
                res.end(chunk, encoding)
                req.url = req.originalUrl || req.url

                const template = _.template(this.msgFormat, {
                    interpolate: /\{\{(.+?)\}\}/g,
                })
                const msg = template({ req, res })
                const level = this.levelFromStatus(req, res)

                this.logger.log(level, msg)
            }
            next()
        }
    }
}
