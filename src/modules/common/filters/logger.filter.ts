import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common'
import * as _ from 'lodash'
import { LoggerService } from 'modules/common/services/logger.service'
import * as winston from 'winston'

@Catch()
export class LoggerFilter implements ExceptionFilter {
    private requestWhitelist = ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query']
    private responseWhitelist = ['statusCode']

    constructor(private logger: LoggerService) {}

    filter(req, propName) {
        return req[propName]
    }

    filterObject(originalObj, whiteList, initialFilter) {
        const obj = {}
        let fieldsSet = false;

        [].concat(whiteList).forEach((propName) => {
            const value = initialFilter(originalObj, propName)

            if (typeof (value) !== 'undefined') {
                obj[propName] = value
                fieldsSet = true
            }
        })

        return fieldsSet ? obj : undefined
    }

    catch(err: Error, host: ArgumentsHost) {
        const ctx = host.switchToHttp()
        const req = ctx.getRequest()
        const res = ctx.getResponse()

        const statusCode = (err instanceof HttpException)
            ? err.getStatus()
            : HttpStatus.INTERNAL_SERVER_ERROR
        const json: any = (err instanceof HttpException)
            ? err.getResponse()
            : {
                statusCode,
                error: 'Internal Server Error',
                message: err.message,
            }

        if (statusCode >= 500) {
            json.stack = err.stack

            const meta: any = winston.exception.getAllInfo(err)
            meta.req = this.filterObject(req, this.requestWhitelist, this.filter)
            meta.res = this.filterObject(res, this.responseWhitelist, this.filter)
            this.logger.error( err.name + ':' + err.message, meta)
        }
        res.status(statusCode).json(json)
    }
}
