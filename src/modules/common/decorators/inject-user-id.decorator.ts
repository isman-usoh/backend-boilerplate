import { createParamDecorator } from '@nestjs/common'

export const InjectUserId = createParamDecorator((data, req) => {
    return req.user.id
})
