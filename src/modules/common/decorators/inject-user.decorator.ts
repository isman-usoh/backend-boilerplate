import { createParamDecorator } from '@nestjs/common'

export const InjectUser = createParamDecorator((data, req) => {
    return req.user
})
