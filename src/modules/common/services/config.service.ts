import { Injectable } from '@nestjs/common'
import * as R from 'ramda'
import { environment as devEnv } from './../../../environments/environment'
import { environment as propEnv } from './../../../environments/environment.prod'
import { environment as testEnv } from './../../../environments/environment.testing'

const getConfig = () => {
    switch (process.env.NODE_ENV) {
        case 'production':
            return propEnv
        case 'testing':
            return testEnv
        default:
            return devEnv
    }
}
const config = getConfig()

type ConfigType = typeof devEnv & typeof config

@Injectable()
export class ConfigService {
    private readonly config: ConfigType

    constructor() {
        this.config = R.mergeDeepLeft(devEnv, getConfig())
    }

    get(key: string): string {
        return R.path(R.split('.', key), this.config)
    }

    value(): ConfigType {
        return this.config
    }
}
