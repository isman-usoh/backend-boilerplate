import { Injectable } from '@nestjs/common'
import * as winston from 'winston'

@Injectable()
export class LoggerService {
    private logger: winston.LoggerInstance

    constructor() {
        const transports: any[] = [
            new winston.transports.Console({
                json: true,
                colorize: true,
            } as any),
        ]
        this.logger = new winston.Logger({ transports })
    }

    log(level: string, message: string, ...meta: any[]) {
        this.logger.log(level, message, meta)
    }

    info(message: string, ...meta: any[]) {
        this.logger.info(message, meta)
    }

    warn(message: string, ...meta: any[]) {
        this.logger.warn(message, meta)
    }

    error(message: string, ...meta: any[]) {
        this.logger.error(message, meta)
    }

    debug(message: string, meta?: any) {
        this.logger.debug(message, meta)
    }
}
