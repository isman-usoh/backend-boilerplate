import { createConnection } from 'typeorm'

export const databaseServices = [
    {
        provide: 'DbConnection',
        useFactory: async () => createConnection({
            type: 'mysql',
            host: process.env.MYSQL_HOST || 'localhost',
            port: 3306,
            username: process.env.MYSQL_USERNAME || 'root',
            password: process.env.MYSQL_PASSWORD || 'root',
            database: process.env.MYSQL_DATABASE || 'backend_development',
            entities: [
                __dirname + '/../../**/*.entity{.ts,.js}',
            ],
            synchronize: true,
        }),
    },
]
