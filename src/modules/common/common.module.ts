import { Module } from '@nestjs/common'
import { LoggerFilter } from 'modules/common/filters/logger.filter'
import { RolesGuard } from 'modules/common/guards/roles.guard'
import { LoggerMiddleware } from 'modules/common/middlewares/logger.middleware'
import { ValidationPipe } from 'modules/common/pipes/validation.pipe'
import { databaseServices } from 'modules/common/services/database.service'
import { LoggerService } from 'modules/common/services/logger.service'
import { ConfigService } from './services/config.service'

@Module({
    providers: [
        ConfigService,
        LoggerFilter,
        LoggerMiddleware,
        LoggerService,
        ValidationPipe,
        RolesGuard,
        ...databaseServices,
    ],
    exports: [
        ConfigService,
        LoggerFilter,
        LoggerMiddleware,
        LoggerMiddleware,
        LoggerService,
        ValidationPipe,
        ...databaseServices,
    ],
})
export class CommonModule {}
