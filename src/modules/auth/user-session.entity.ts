import * as crypto from 'crypto'
import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm'

const randomToken = () => {
    return crypto.randomBytes(Math.ceil(32))
        .toString('hex')
        .slice(0, 64)
}

@Entity()
export class UserSession {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    userId: number

    @Column()
    accessToken: string

    @Column({ type: 'datetime' })
    expireDate: Date

    constructor() {
        this.accessToken = randomToken()
        this.expireDate = new Date(Date.now() + (3 * 30 * 24 * 60 * 60 * 1000))
    }

    toDTO() {
        const data = this
        return {
            id: data.id,
            accessToken: data.accessToken,
            expireDate: data.expireDate,
        }
    }
}
