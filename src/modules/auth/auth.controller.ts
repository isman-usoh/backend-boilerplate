import { BadRequestException, Body, Controller, HttpCode, HttpStatus, Post, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { AuthDTO, LoginBody } from 'modules/auth/auth.dto'
import { UserSessionService } from 'modules/auth/user-session.service'
import { InjectUser } from 'modules/common/decorators/inject-user.decorator'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { UserService } from 'modules/user/user.service'

@Controller('auth')
@ApiUseTags('Authentication')
export class AuthController {
    constructor(
        private userSvc: UserService,
        private sessionSvc: UserSessionService) { }

    @Post('login')
    @ApiOperation({ title: 'เข้าระบบ', operationId: 'login' })
    @ApiResponse({ status: 200, type: AuthDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async login(@Body() body: LoginBody) {
        const { username, password } = body

        const userEntity = await this.userSvc.getByUsername(username)
        if (!userEntity || !userEntity.verifyPassword(password)) {
            throw new BadRequestException('', 'EmailOrPasswordInvalid')
        }

        const sessionEntity = await this.sessionSvc.create(userEntity.id)
        return sessionEntity.toDTO()
    }

    @Post('logout')
    @HttpCode(201)
    @UseGuards(AuthGuard('bearer'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'ออกระบบ', operationId: 'logout' })
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async logout(@InjectUser() user) {
        const { sid } = user

        await this.sessionSvc.revoke(sid)
    }
}
