import { Module } from '@nestjs/common'
import { AuthController } from 'modules/auth/auth.controller'
import { authProviders } from 'modules/auth/auth.providers'
import { HttpStrategy } from 'modules/auth/http.strategy'
import { UserSessionService } from 'modules/auth/user-session.service'
import { UserModule } from 'modules/user/user.module'

@Module({
    imports: [
       UserModule,
    ],
    controllers: [
        AuthController,
    ],
    providers: [
        HttpStrategy,
        UserSessionService,
        ...authProviders,
    ],
})
export class AuthModule {

}
