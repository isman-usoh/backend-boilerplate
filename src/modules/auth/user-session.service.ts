import { Inject, Injectable } from '@nestjs/common'
import { UserSession } from 'modules/auth/user-session.entity'
import { UserService } from 'modules/user/user.service'
import { Repository } from 'typeorm'

@Injectable()
export class UserSessionService {
    constructor(
        @Inject('UserSessionRepository')
        private readonly sessionRepo: Repository<UserSession>,
        private readonly userSvc: UserService,
    ) { }

    async getById(id: number) {
        const docs = await this.sessionRepo.findByIds([id])
        if (!docs || docs.length === 0) {
            return
        }
        return docs[0]
    }

    async validateUser(accessToken: string) {
        const docs = await this.sessionRepo.find({ where: { accessToken }})
        if (!docs || docs.length === 0) {
            return
        }
        const userId = docs[0].userId
        return this.userSvc.getById(userId)
    }

    async create(userId: number) {
        const newSession = new UserSession()
        newSession.userId = userId
        return await this.sessionRepo.save(newSession)
    }

    async revoke(id: number) {
        await this.sessionRepo.delete(id)
    }
}
