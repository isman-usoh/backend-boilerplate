import { ApiModelProperty } from '@nestjs/swagger'

export class LoginBody {
    @ApiModelProperty()
    username: string
    @ApiModelProperty()
    password: string
}

export class AuthDTO {
    @ApiModelProperty()
    id: number
    @ApiModelProperty()
    accessToken: string
    @ApiModelProperty({ type: 'string', format: 'date-time'})
    expireDate: Date
}
