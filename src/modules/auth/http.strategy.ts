import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { UserSessionService } from 'modules/auth/user-session.service'
import { Strategy } from 'passport-http-bearer'

@Injectable()
export class HttpStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly sessionSvc: UserSessionService) {
    super()
  }

  async validate(token: string) {
    const user = await this.sessionSvc.validateUser(token)
    if (!user) {
      throw new UnauthorizedException()
    }
    return user.toDTO()
  }
}
