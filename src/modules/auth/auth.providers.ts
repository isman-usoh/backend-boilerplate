import { UserSession } from 'modules/auth/user-session.entity'
import { Connection } from 'typeorm'

export const authProviders = [
    {
        provide: 'UserSessionRepository',
        useFactory: (connection: Connection) => connection.getRepository(UserSession),
        inject: ['DbConnection'],
    },
]
