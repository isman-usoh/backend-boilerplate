import { Module } from '@nestjs/common'
import { CommonModule } from 'modules/common/common.module'
import { CampaignController } from 'modules/event/campaign.controller'
import { CampaignService } from 'modules/event/campaign.service'
import { EventController } from 'modules/event/event.controller'
import { eventProviders } from 'modules/event/event.providers'
import { EventService } from 'modules/event/event.service'

@Module({
    imports: [
        CommonModule,
    ],
    controllers: [
        EventController,
        CampaignController,
    ],
    providers: [
        CampaignService,
        EventService,
        ...eventProviders,
    ],
})
export class EventModule {

}
