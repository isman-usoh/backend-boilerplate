import { BadRequestException, Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { CreateEventBody, EventDTO, UpdateEventBody } from 'modules/event/event.dto'
import { EventService } from 'modules/event/event.service'
import { UpdateEvent } from 'typeorm'

@Controller('events')
// @UseGuards(AuthGuard('bearer'))
// @ApiBearerAuth()
@ApiUseTags('Event')
export class EventController {
    constructor(private readonly eventSvc: EventService) {}

    @Get()
    @HttpCode(200)
    @ApiOperation({ title: 'ดึงอีเวนต์ทั้งหมด', operationId: 'find_events' })
    @ApiResponse({ status: 200, type: EventDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async find() {
        const entities = await this.eventSvc.find()
        return entities.map(entity => entity.toDTO())
    }

    @Get(':id')
    @HttpCode(200)
    @ApiOperation({ title: 'ดึงอีเวนต์', operationId: 'get_event' })
    @ApiResponse({ status: 200, type: EventDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async get(@Param('id') id: number) {
        const entity = await this.eventSvc.getById(id)
        if (entity) {
            throw new BadRequestException('', 'CampaignNotFound')
        }
        return entity.toDTO()
    }

    @Post()
    @HttpCode(201)
    @ApiOperation({ title: 'สร้างอีเวนต์ใหม่', operationId: 'create_event' })
    @ApiResponse({ status: 201, type: EventDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async create(@Body() body: CreateEventBody) {
        const { name, slogan, description, startDate, endDate } = body
        const entity = await this.eventSvc.create(name, slogan, description, startDate, endDate)
        return entity.toDTO()
    }

    @Patch(':id')
    @HttpCode(204)
    @ApiOperation({ title: 'แก้ไขข้อมูลอีเวนต์', operationId: 'update_event' })
    @ApiResponse({ status: 204 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async update(@Param('id') id: number, @Body() body: UpdateEventBody) {
        const { name, slogan, description, startDate, endDate } = body
        await this.eventSvc.update(id, name, slogan, description, startDate, endDate)
    }

    @Patch(':id/primary')
    @HttpCode(204)
    @ApiOperation({ title: 'ตั้งเป็นอีเวนต์หลัก', operationId: 'make_primary_event'  })
    @ApiResponse({ status: 204 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async makePrimary(@Param('id') id: number) {
        await this.eventSvc.makePrimary(id)
    }

    @Delete(':id')
    @HttpCode(204)
    @ApiOperation({ title: 'ลบอีเวนต์', operationId: 'delete_event' })
    @ApiResponse({ status: 204 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async delete(@Param('id') id: number) {
        await this.eventSvc.delete(id)
    }
}
