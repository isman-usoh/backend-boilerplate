import { Event } from 'modules/event/event.entity'
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'

export enum CampaignRef {
    CAR = 'CAR',
    MOTORCYCLE = 'MOTORCYCLE',
    OTHER = 'OTHER',
}

@Entity()
export class Campaign {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length: 64 })
    name: string

    @Column({ length: 16 })
    ref: CampaignRef

    @Column()
    startDate: Date

    @Column()
    endDate: Date

    @Column()
    enabled: boolean = true

    @Column({ type: 'text' })
    condition?: string

    @ManyToOne(() => Event, event => event.campaigns)
    event: Event

    toDTO() {
        const entity = this
        return {
            id: entity.id,
            name: entity.name,
            startDate: entity.startDate,
            endDate: entity.endDate,
            condition: entity.condition,
        }
    }
}
