import { BadRequestException, Body, Controller, Delete, Get, HttpCode, Param, Patch, Post } from '@nestjs/common'
import { ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { CampaignDTO, CreateCampaignBody, UpdateCampaignBody } from 'modules/event/campaign.dto'
import { CampaignService } from 'modules/event/campaign.service'

@Controller('campaigns')
// @UseGuards(AuthGuard('bearer'))
// @ApiBearerAuth()
@ApiUseTags('Campaign')
export class CampaignController {
    constructor(private readonly campaignSvc: CampaignService) {}

    @Get()
    @HttpCode(200)
    @ApiOperation({ title: 'ดึง Campaign ทั้งหมด', operationId: 'find_campaigns' })
    @ApiResponse({ status: 200, type: CampaignDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async find() {
        const entities = await this.campaignSvc.find()
        return entities.map(entity => entity.toDTO())
    }

    @Get(':id')
    @HttpCode(200)
    @ApiOperation({ title: 'ดึง Campaign', operationId: 'get_campaign' })
    @ApiResponse({ status: 200, type: CampaignDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async get(@Param('id') id: number) {
        const entity = await this.campaignSvc.getById(id)
        if (entity) {
            throw new BadRequestException('', 'CampaignNotFound')
        }
        return entity.toDTO()
    }

    @Post()
    @HttpCode(204)
    @ApiOperation({ title: 'สร้าง Campaign ใหม่', operationId: 'create_campaign' })
    @ApiResponse({ status: 204, type: CampaignDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async create(@Body() body: CreateCampaignBody) {
        const { name, ref, condition, startDate, endDate } = body
        const entity = await this.campaignSvc.create(name, ref, condition, startDate, endDate)
        return entity.toDTO()
    }

    @Patch(':id')
    @HttpCode(201)
    @ApiOperation({ title: 'แก้ไขข้อมูล Campaign', operationId: 'update_campaign' })
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async update(@Param('id') id: number, @Body() body: UpdateCampaignBody) {
        const { name, condition, startDate, endDate } = body
        await this.campaignSvc.update(id, name, condition, startDate, endDate)
    }

    @Delete(':id')
    @HttpCode(201)
    @ApiOperation({ title: 'ลบ Campaign', operationId: 'delete_campaign' })
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async delete(@Param('id') id: number) {
        await this.campaignSvc.delete(id)
    }
}
