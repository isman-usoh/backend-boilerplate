import { Inject, Injectable } from '@nestjs/common'
import { Event } from 'modules/event/event.entity'
import { Repository } from 'typeorm'

@Injectable()
export class EventService {
    constructor(
        @Inject('EventRepository')
        private readonly eventRepo: Repository<Event>,
    ) { }

    async find() {
        return await this.eventRepo.find()
    }

    async getById(id: number) {
        const entities = await this.eventRepo.findByIds([id])
        if (!entities || entities.length === 0) {
            return
        }
        return entities[0]
    }

    async create(name: string, slogan: string, description?: string, startDate?: Date, endDate?: Date) {
        const newEvent = new Event()
        newEvent.name = name
        newEvent.slogan = slogan
        newEvent.description = description
        newEvent.startDate = startDate
        newEvent.endDate = endDate
        return this.eventRepo.save(newEvent)
    }

    async update(id: number, name: string, slogan: string, description?: string, startDate?: Date, endDate?: Date) {
        await this.eventRepo.update(id, { name, slogan, description, startDate, endDate })
    }

    async makePrimary(id: number) {
        await this.eventRepo.update(id, { primary: true })
    }

    async delete(id: number) {
        await this.eventRepo.delete(id)
    }
}
