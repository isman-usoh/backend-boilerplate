
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger'

export class CreateEventBody {
    @ApiModelProperty()
    name: string
    @ApiModelProperty()
    slogan: string
    @ApiModelPropertyOptional()
    description?: string
    @ApiModelPropertyOptional({ type: 'string', format: 'date-time'})
    startDate?: Date
    @ApiModelPropertyOptional({ type: 'string', format: 'date-time'})
    endDate?: Date
}

export class UpdateEventBody extends CreateEventBody{
}

export class EventDTO extends CreateEventBody{
    @ApiModelProperty()
    id: number

    @ApiModelProperty()
    primary: boolean
}
