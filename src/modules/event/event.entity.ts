import { Campaign } from 'modules/event/campaign.entity'
import { EventDTO } from 'modules/event/event.dto'
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Event {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length: 64 })
    name: string

    @Column({ length: 256 })
    slogan: string

    @Column({ length: 512 })
    description?: string

    @Column()
    startDate?: Date

    @Column()
    endDate?: Date

    @Column()
    primary: boolean = false

    @OneToMany(() => Campaign, campaign => campaign.event)
    campaigns: Campaign[]

    toDTO(): EventDTO {
        const entity = this
        return {
            id: entity.id,
            name: entity.name,
            slogan: entity.slogan,
            description: entity.description,
            startDate: entity.startDate,
            endDate: entity.endDate,
            primary: entity.primary,
        }
    }
}
