import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger'
import { CampaignRef } from 'modules/event/campaign.entity'

export class UpdateCampaignBody  {
    @ApiModelProperty()
    name: string
    @ApiModelProperty()
    startDate: Date
    @ApiModelProperty()
    endDate: Date
    @ApiModelPropertyOptional()
    condition?: string
}

export class CreateCampaignBody extends UpdateCampaignBody {
    @ApiModelProperty({ type: 'string', enum: Object.keys(CampaignRef)})
    ref: CampaignRef
}

export class CampaignDTO extends CreateCampaignBody {
    @ApiModelProperty()
    id: number
    @ApiModelProperty()
    enabled: boolean = true
}
