import { Campaign } from 'modules/event/campaign.entity'
import { Event } from 'modules/event/event.entity'
import { Connection } from 'typeorm'

export const eventProviders = [
    {
        provide: 'EventRepository',
        useFactory: (connection: Connection) => connection.getRepository(Event),
        inject: ['DbConnection'],
    },
    {
        provide: 'CampaignRepository',
        useFactory: (connection: Connection) => connection.getRepository(Campaign),
        inject: ['DbConnection'],
    },
]
