import { Inject, Injectable } from '@nestjs/common'
import { Campaign, CampaignRef } from 'modules/event/campaign.entity'
import { Repository } from 'typeorm'

@Injectable()
export class CampaignService {

    constructor(
        @Inject('CampaignRepository')
        private readonly campaignRepo: Repository<Campaign>,
    ) {}

    async find() {
        return await this.campaignRepo.find()
    }

    async getById(id: number) {
        const entities = await this.campaignRepo.findByIds([id])
        if (!entities || entities.length === 0) {
            return
        }
        return entities[0]
    }

    async create(name: string, ref: CampaignRef, condition: string, startDate: Date, endDate: Date) {
        const newEvent = new Campaign()
        newEvent.name = name
        newEvent.ref = ref
        newEvent.condition = condition
        newEvent.startDate = startDate
        newEvent.endDate = endDate
        return this.campaignRepo.save(newEvent)
    }

    async update(id: number, name: string, condition: string, startDate: Date, endDate: Date) {
        await this.campaignRepo.update(id, { name, condition, startDate, endDate })
    }

    async delete(id: number) {
        await this.campaignRepo.delete(id)
    }

}
