import { BadGatewayException, Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiBearerAuth, ApiImplicitQuery, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { CreateUserBody, UpdateUserBody, UserDTO } from 'modules/user/user.dto'
import { UserService } from 'modules/user/user.service'

@Controller('users')
// @UseGuards(AuthGuard('bearer'))
// @ApiBearerAuth()
@ApiUseTags('User')
export class UserController {

    constructor(
        private userSvc: UserService) { }

    @Get()
    @ApiOperation({ title: 'ดึงข้อมูลผู้ใช้ทั้งหมด', operationId: 'find_users' })
    @ApiImplicitQuery({ name: 'q', required: false })
    @ApiImplicitQuery({ name: 'offet', required: false })
    @ApiImplicitQuery({ name: 'limit', required: false })
    @ApiResponse({ status: 200, type: UserDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async find(@Query('q') query?: string,
               @Query('offet') offset?: number,
               @Query('limit') limit?: number) {
        const entities = await this.userSvc.find(query, offset, limit)
        return entities.map(entity => entity.toDTO())
    }

    @Get(':id')
    @ApiOperation({ title: 'ดึงข้อมูลผู้ใช้', operationId: 'get_user' })
    @ApiResponse({ status: 200, type: UserDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async getBy(@Param('id') id: number) {
        const entity = await this.userSvc.getById(id)
        if (!entity) {
            throw new BadGatewayException('', 'UserNotFound')
        }
        return entity.toDTO()
    }

    @Post()
    @ApiOperation({ title: 'สร้างผู้ใช้ใหม่', operationId: 'create_user' })
    @ApiResponse({ status: 200, type: UserDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async create(@Body() body: CreateUserBody) {
        const { username, password, firstName, lastName, email, phone, mobile } = body
        const entity = await this.userSvc.create(username, password, firstName, lastName, email, phone, mobile)
        return entity.toDTO()
    }

    @Patch(':id')
    @HttpCode(201)
    @ApiOperation({ title: 'แก้ไขข้อมูลผู้ใช้', operationId: 'update_user' })
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async update(@Param('id') id: number, @Body() body: UpdateUserBody) {
        const { firstName, lastName, email, phone, mobile } = body
        await this.userSvc.updateProfile(id, firstName, lastName, email, phone, mobile)
    }

    @Delete(':id')
    @HttpCode(201)
    @ApiOperation({ title: 'ลบผู้ใช้', operationId: 'delete_user'  })
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async delete(@Param('id') id: number) {
        await this.userSvc.delete(id)
    }
}
