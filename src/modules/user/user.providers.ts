import { UserGroup } from 'modules/user/user-group.entity'
import { User } from 'modules/user/user.entity'
import { Connection } from 'typeorm'

export const userProviders = [
    {
        provide: 'UserRepository',
        useFactory: (connection: Connection) => connection.getRepository(User),
        inject: ['DbConnection'],
    },
    {
        provide: 'UserGroupRepository',
        useFactory: (connection: Connection) => connection.getRepository(UserGroup),
        inject: ['DbConnection'],
    },
]
