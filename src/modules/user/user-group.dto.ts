import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger'
import { CreateUserBody } from 'modules/user/user.dto'

export class UpdateUserGroupBody {
    @ApiModelProperty()
    name: string
    @ApiModelPropertyOptional()
    remask?: string
    @ApiModelProperty({ type: 'string', isArray: true })
    roles: string[]
    @ApiModelProperty()
    enabled: boolean
}

export class CreateUserGroupBody extends UpdateUserGroupBody {

}

export class ImportUserBody {
    @ApiModelProperty({ type: CreateUserBody, isArray: true })
    items: CreateUserBody[]
}

export class UserGroupDTO extends UpdateUserGroupBody {
    @ApiModelProperty()
    id: number
}
