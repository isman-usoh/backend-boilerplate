import { Module } from '@nestjs/common'
import { CommonModule } from 'modules/common/common.module'
import { MeController } from 'modules/user/me.controller'
import { UserGroupController } from 'modules/user/user-group.controller'
import { UserGroupService } from 'modules/user/user-group.service'
import { UserController } from 'modules/user/user.controller'
import { userProviders } from 'modules/user/user.providers'
import { UserService } from 'modules/user/user.service'

@Module({
    imports: [
        CommonModule,
    ],
    controllers: [
        MeController,
        UserController,
        UserGroupController,
    ],
    providers: [
        UserService,
        UserGroupService,
        ...userProviders,
    ],
    exports: [
        UserService,
        UserGroupService,
        ...userProviders,
    ],
})
export class UserModule {

}
