import { BadRequestException, Inject, Injectable } from '@nestjs/common'
import { UserGroup } from 'modules/user/user-group.entity'
import { Repository } from 'typeorm'

@Injectable()
export class UserGroupService {
    constructor(
        @Inject('UserGroupRepository')
        private readonly groupRepo: Repository<UserGroup>,
    ) { }

    async find(query: string, offset = 0, limit = 50) {
        let builder = this.groupRepo
            .createQueryBuilder('group')
        if (query) {
            builder = builder.where('group.name LIKE %:query%', { query })
        }
        return builder
            .offset(offset)
            .limit(limit)
            .getMany()
    }

    async getById(id: number) {
        const docs = await this.groupRepo.findByIds([id])
        if (!docs || docs.length === 0) {
            return
        }
        return docs[0]
    }

    async create(name: string, roles: string[], remark?: string) {
        const newGroup = new UserGroup()
        newGroup.name = name
        newGroup.roles = roles.join(',')
        newGroup.remask = remark
        return this.groupRepo.save(newGroup)
    }

    async update(id: number, name: string, roles: string[], enabled: boolean, remark?: string) {
        const entity = await this.getById(id)
        if (!entity) {
            throw new BadRequestException('', 'UserGroupNotFound')
        }
        entity.name = name
        entity.roles = roles.join(',')
        entity.remask = remark
        entity.enabled = enabled
        this.groupRepo.save(entity)
    }

    async delete(id: number) {
        await this.groupRepo.delete(id)
    }
}
