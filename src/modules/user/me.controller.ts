import { BadRequestException, Body, Controller, Delete, Get, HttpCode, Patch, Post, Req, Session, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { InjectUserId } from 'modules/common/decorators/inject-user-id.decorator'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { UpdatePasswordBody, UpdateUserBody, UserDTO } from 'modules/user/user.dto'
import { UserService } from 'modules/user/user.service'

@Controller('me')
@UseGuards(AuthGuard('bearer'))
@ApiBearerAuth()
@ApiUseTags('Me')
export class MeController {
    constructor(
        private readonly userSvc: UserService,
    ) {}

    @Get()
    @HttpCode(200)
    @ApiOperation({ title: 'ข้อมูลผู้ใช้ปัจจุบัน', operationId: 'info_me' })
    @ApiResponse({ status: 200, type: UserDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async info(@InjectUserId() userId) {
        const userDoc = await this.userSvc.getById(userId)
        if (!userDoc) {
            throw new BadRequestException('', 'UserNotFound')
        }
        return userDoc.toDTO()
    }

    @Patch()
    @HttpCode(204)
    @ApiOperation({ title: 'แก้ไขข้อมูลผู้ใช้ปัจจุบัน', operationId: 'update_profile_me' })
    @ApiResponse({ status: 204 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async updateProfile(@InjectUserId() userId, @Body() body: UpdateUserBody) {
        const { firstName, lastName, email, phone, mobile } = body

        this.userSvc.updateProfile(userId, firstName, lastName, email, phone, mobile)
    }

    @Patch('update-password')
    @HttpCode(204)
    @ApiOperation({ title: 'แก้ไขรหัสผ่านผู้ใช้ปัจจุบัน', operationId: 'update_password_me' })
    @ApiResponse({ status: 204 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async updatePassword(@InjectUserId() userId, @Body() body: UpdatePasswordBody) {
        const { password, newPassword } = body

        const passwordValid = await this.userSvc.verifyPassword(userId, password)
        if (!passwordValid) {
            throw new BadRequestException('', 'PasswordInvalid')
        }

        this.userSvc.updatePassword(userId, newPassword)
    }
}
