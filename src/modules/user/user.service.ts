import { BadRequestException, Inject, Injectable } from '@nestjs/common'
import { User } from 'modules/user/user.entity'
import { Repository } from 'typeorm'

@Injectable()
export class UserService {
    constructor(
        @Inject('UserRepository')
        private readonly userRepo: Repository<User>,
    ) { }

    async find(query: string, offset = 0, limit = 50) {
        let builder = this.userRepo
            .createQueryBuilder('user')
        if (query) {
            builder = builder.where('user.username LIKE %:query%', { query })
                .orWhere('user.firstName LIKE %:query%', { query })
                .orWhere('user.lastName LIKE %:query%', { query })
        }
        return builder
            .offset(offset)
            .limit(limit)
            .getMany()
    }

    async getById(id: number) {
        const entities = await this.userRepo.findByIds([id])
        if (!entities || entities.length === 0) {
            return
        }
        return entities[0]
    }

    async getByUsername(username: string) {
        const entities = await this.userRepo.find({ where: { username } })
        if (!entities || entities.length === 0) {
            return
        }
        return entities[0]
    }

    async create(username, password, firstName, lastName, email, phone, mobile) {
        this.userRepo.create()
        const newUser = new User()
        newUser.username = username
        newUser.password = password
        newUser.firstName = firstName
        newUser.lastName = lastName
        newUser.email = email
        newUser.phone = phone
        newUser.mobile = mobile

        return this.userRepo.save(newUser)
    }

    async updateProfile(id: number, firstName?: string, lastName?: string,
                        email?: string, phone?: string, mobile?: string) {
        await this.userRepo.update(id, { firstName, lastName, email, phone, mobile })
    }

    async updatePassword(id: number, password: string) {
        await this.userRepo.update(id, { password })
    }

    async verifyPassword(id: number, password: string) {
        const userEntity = await this.getById(id)
        if (!userEntity) {
            throw new BadRequestException('', 'UserNotFound')
        }
        return userEntity.verifyPassword(password)
    }

    async delete(id: number) {
        if (id === 1) {
             throw new BadRequestException('', 'CannotDeleteInitialAdmin')
        }
        await this.userRepo.delete(id)
    }
}
