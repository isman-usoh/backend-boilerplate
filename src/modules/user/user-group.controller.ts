import { BadRequestException, Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiBearerAuth, ApiModelProperty, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger'
import { ErrorDTO } from 'modules/common/dtos/error.dto'
import { CreateUserGroupBody, ImportUserBody, UpdateUserGroupBody, UserGroupDTO } from 'modules/user/user-group.dto'
import { UserGroupService } from 'modules/user/user-group.service'

@Controller('user-groups')
// @UseGuards(AuthGuard('bearer'))
// @ApiBearerAuth()
@ApiUseTags('UserGroup')
export class UserGroupController {
    constructor(
        private groupSvc: UserGroupService) { }

    @Get()
    @ApiOperation({ title: 'ดึงข้อมูลกลุ่มผู้ใช้ทั้งหมด'})
    @ApiResponse({ status: 200, type: UserGroupDTO, isArray: true })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async find(@Query('q') query: string = '',
               @Query('offet') offset: number = 0,
               @Query('limit') limit: number = 50) {
        const entities = await this.groupSvc.find(query, offset, limit)
        return entities.map(entity => entity.toDTO())
    }

    @Get(':id')
    @ApiOperation({ title: 'ดึงข้อมูลกลุ่มผู้ใช้'})
    @ApiResponse({ status: 200, type: UserGroupDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async getBy(@Param('id') id: number) {
        const entity = await this.groupSvc.getById(id)
        if (!entity) {
            throw new BadRequestException('', 'UserGroupNotFound')
        }
        return entity.toDTO()
    }

    @Post()
    @ApiOperation({ title: 'นำเข้าข้อมูลผู้ใช้'})
    async import(@Body() body: ImportUserBody[]) {

    }

    @Post()
    @ApiOperation({ title: 'สร้างกลุ่มผู้ใช้ใหม่'})
    @ApiResponse({ status: 200, type: UserGroupDTO })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async create(@Body() body: CreateUserGroupBody) {
        const { name, roles, remask } = body
        const entity = await this.groupSvc.create(name, roles, remask)
        return entity.toDTO()
    }

    @Patch(':id')
    @HttpCode(201)
    @ApiOperation({ title: 'แก้ไขข้อมูลกลุ่มผู้ใช้'})
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async update(@Param('id') id: number, @Body() body: UpdateUserGroupBody) {
        const { name, roles, enabled, remask } = body
        await this.groupSvc.update(id, name, roles, enabled, remask)
    }

    @Delete(':id')
    @HttpCode(201)
    @ApiOperation({ title: 'ลบกลุ่มผู้ใช้'})
    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 'default' as any, type: ErrorDTO })
    async delete(@Param('id') id: number) {
        await this.groupSvc.delete(id)
    }
}
