import { UserGroup } from 'modules/user/user-group.entity'
import { UserDTO } from 'modules/user/user.dto'
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length: 16, unique: true })
    username: string
    @Column({ length: 32 })
    password: string
    @Column()
    enabled: boolean = true

    @Column({ length: 32, nullable: true })
    firstName?: string
    @Column({ length: 32, nullable: true })
    lastName?: string
    @Column({ length: 64, nullable: true })
    email?: string
    @Column({ length: 12, nullable: true })
    phone?: string
    @Column({ length: 12, nullable: true })
    mobile?: string

    @Column({ length: 128, nullable: true })
    remask?: string
    @ManyToOne(type => UserGroup, group => group.users)
    group: User

    verifyPassword(password: string) {
        return this.password === password
    }

    toDTO(): UserDTO {
        const entity = this
        return {
            id: entity.id,
            username: entity.username,
            enabled: entity.enabled,

            firstName: entity.firstName,
            lastName: entity.lastName,
            email: entity.email,
            phone: entity.phone,
            mobile: entity.mobile,
            remask: entity.remask,
        }
    }
}
