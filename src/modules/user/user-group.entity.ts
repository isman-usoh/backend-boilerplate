import { UserGroupDTO } from 'modules/user/user-group.dto'
import { User } from 'modules/user/user.entity'
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class UserGroup {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length: 32, nullable: false })
    name: string
    @Column({ length: 128, nullable: true })
    remask?: string
    @Column({})
    roles: string
    @Column()
    enabled: boolean

    @OneToMany(type => User, user => user.group)
    users: User[]

    toDTO(): UserGroupDTO {
        const data = this
        return {
            id: data.id,
            name: data.name,
            remask: data.remask,
            roles: data.roles.split(','),
            enabled: data.enabled,
        }
    }
}
