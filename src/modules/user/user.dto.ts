import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger'

export class UpdateUserBody {
    @ApiModelPropertyOptional()
    firstName?: string
    @ApiModelPropertyOptional()
    lastName?: string
    @ApiModelPropertyOptional()
    email?: string
    @ApiModelPropertyOptional()
    phone?: string
    @ApiModelPropertyOptional()
    mobile?: string
    @ApiModelPropertyOptional()
    remask?: string
}

export class CreateUserBody extends UpdateUserBody {
    @ApiModelProperty()
    username: string
    @ApiModelProperty()
    password: string
}

export class UpdatePasswordBody {
    @ApiModelProperty()
    password: string
    @ApiModelProperty()
    newPassword: string
}

export class UserDTO extends UpdateUserBody{
    @ApiModelProperty()
    id: number
    @ApiModelProperty()
    username: string
    @ApiModelProperty()
    enabled: boolean
}
