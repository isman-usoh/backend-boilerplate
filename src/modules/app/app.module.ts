import { Module } from '@nestjs/common'
import { AdminModule } from 'modules/admin/admin.module'
import { AuthModule } from 'modules/auth/auth.module'
import { EventModule } from 'modules/event/event.module'
import { PreemptionModule } from 'modules/preemption/preemption.module'
import { UserModule } from 'modules/user/user.module'

@Module({
    imports: [
        AdminModule,
        AuthModule,
        EventModule,
        UserModule,
        PreemptionModule,
    ],
})
export class AppModule { }
