#!/bin/bash

set -e

export GCLOUD_PROJECT_ID=${GCLOUD_PROJECT_ID:-isman-app}
export GCLOUD_CLUSTER_NAME=${GCLOUD_CLUSTER_NAME:-isman-staging-cluster}
export GCLOUD_COMPUTE_ZONE=${GCLOUD_COMPUTE_ZONE:-asia-southeast1-b}
export APP_NAME=${APP_NAME:-isman-backend}
export APP_VERSION=$(cat package.json|grep version|head -1|awk -F: '{ print $2 }'|sed 's/[\",]//g'|xargs)
export APP_MAJOR_VERSION="v"$(echo $APP_VERSION|tr -d '[[:space:]]'|cut -d'.' -f1)
export APP_ENV=${APP_ENV:-staging}
export APP_HOST=${APP_HOST:-isman.com}
export BUILD_VERSION=$(date +%Y-%m-%d).$CIRCLE_BUILD_NUM
export IMAGE_NAME=asia.gcr.io/${GCLOUD_PROJECT_ID}/${APP_NAME}-${APP_ENV}
export APP_BUILD_VERSION=${APP_VERSION}-${BUILD_VERSION}

mkdir k8s-dist

for f in k8s/*.yaml
do
	envsubst < $f > "k8s-dist/gen-$(basename $f)"
done

printenv
find ./k8s-dist -name '*.yaml' -exec cat {} \;

echo $GCLOUD_SERVICE_ACCOUNT | base64 -d > ${HOME}/account-auth.json
gcloud auth activate-service-account --key-file ${HOME}/account-auth.json
gcloud auth configure-docker

docker build --cache-from ${IMAGE_NAME}:latest -t ${IMAGE_NAME}:latest -t ${IMAGE_NAME}:${BUILD_VERSION} --rm=false .
docker push ${IMAGE_NAME}:${BUILD_VERSION}
docker push ${IMAGE_NAME}:latest

gcloud --quiet config set project $GCLOUD_PROJECT_ID
gcloud --quiet config set compute/zone ${GCLOUD_COMPUTE_ZONE}
gcloud --quiet config set container/use_client_certificate True
gcloud --quiet config set container/cluster ${GCLOUD_CLUSTER_NAME}
gcloud --quiet container clusters get-credentials ${GCLOUD_CLUSTER_NAME}
kubectl apply -f ./k8s-dist --record